# Default Class

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## Introduction

Default Class provides classes based on information Drupal knows about.

- Adds region machine names as classes on blocks.
- Adds a class on any node with one individual piece of content.
- Adds a class on any user with one individual piece of content.
- Adds a class on any term with one individual piece of content.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/default_class).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/default_class).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Maintainers

Current maintainers:

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
- Radheshyam Kumawat (radheymkumar) - https://www.drupal.org/u/radheymkumar
